'use strict';


module.exports = {
    server: {
        port: 3000
    },
    register: {
        plugins: [
            {
                plugin:'./web/router/auth'
            },
            {
                plugin: './web/router/article',
                routes: {
                    prefix: '/api/article'
                }
            },
            {
                plugin:'./web/router/user',
                routes: {
                    prefix: '/api/user'
                }
            }
        ]
    }
};
