const Bcrypt = require('bcryptjs');

const users = {
    john: {
        username: 'john',
        password: 'john',   // 'secret'
        name: 'John Doe',
        id: '2133d32a'
    }
};

module.exports.validate = async (request, username, password, h) => {

    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    console.log("username",username,password)

    const isValid = password === user.password;
    const credentials = { id: user.id, username: user.username };

    return { isValid, credentials };
};