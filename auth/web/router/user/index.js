const service = require('./service');
const Joi = require('joi');

exports.register = (server) =>{

    server.route({
        method:'get',
        path:"/",
        handler:service.list
    });

    server.route({
        method:'POST',
        path:"/",
        config:{
            validate:{
                payload:{
                    email: Joi.string().email().min(2).max(30).required(),
                    username: Joi.string().required(),
                    password: Joi.string().required()
                }
            },
            handler:service.create
        },
        
    });

    server.route({
        method:'POST',
        path:"/login",
        handler:service.login_user
        
    });

    // server.route({
    //     method:'GET',
    //     path:"/",
    //     handler:service.get
    // });

    // server.route({
    //     method:'PUT',
    //     path:"/",
    //     handler:service.update
    // });

    // server.route({
    //     method:'DELETE',
    //     path:"/",
    //     handler:service.list
    // });
}

exports.pkg = {
    name:"user"
}