const service = require("./service"); 
const Joi = require('joi');

exports.register = (server) => {
    server.route({
        method:"GET",
        path:'/',
        handler:service.list
    })

    server.route({
        method:"POST",
        path:'/',
        config:{
            auth:'simple',
            validate:{
                payload:{
                    name:Joi.string().min(3).max(50).required(),
                    title: Joi.string().min(3).max(50).required(),
                    subtitle: Joi.string().optional(),
                    author: Joi.string().required()
                },
                failAction:(request, h, error) => {
                    throw error;
                }
            },
            handler:service.create
        }
        
    })

    server.route({
        method:"GET",
        path:'/{id}',
        handler:service.get
    })

    server.route({
        method:"PUT",
        path:'/{id}',
        handler:service.update
    })

    server.route({
        method:"DELETE",
        path:'/{id}',
        handler:service.delete
    })
}

exports.pkg = {
    name:"article"
}