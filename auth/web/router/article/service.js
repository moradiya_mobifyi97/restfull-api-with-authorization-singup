const Article = require('../../models/article');

exports.list = (req, h) =>{
    return Article.find().exec().then((doc) => {
        return { doc:doc }
    }).catch((err) => {
        return { err:err }
    })
}

exports.create = (req, h) =>{
    const article = {
        name:req.payload.name,
        title:req.payload.title,
        subtitle:req.payload.subtitle,
        author:req.payload.author
    }
    return Article.create(article).then((doc) => {
         return { message:"Article create Successfully", doc:doc };
        
    }).catch((err) => {
        return { err:err }
    });
}

exports.get = (req, h) =>{
    return Article.findById(req.params.id).exec()
    .then( (result) => {
        if(!result) return { message: "not Found" }
        return { result:result }
    })
}

exports.update = (req, h) =>{
    return Article.findById(req.params.id).exec()
    .then( (result) => {

        result.name = req.payload.name;
        result.title = req.payload.title;
        result.subtitle = req.payload.subtitle;
        result.author = req.payload.author;

        result.save();
    }).then((doc) => {
        return { message:"Article data update successfull", article:result }
    }).catch((err) => {
        return { err:err }
    })
}

exports.delete = (req, h) =>{
    return Article.findById(req.params.id).exec(function(err, doc){
        if(err) return { err:err };
        if(!doc) return { message:"Article not Found" }
        doc.remove(function(err) {
            if(err) return { err:err };
            return { message: "Article delete successfull" }
        })
    })
}