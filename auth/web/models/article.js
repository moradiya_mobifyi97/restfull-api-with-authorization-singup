const mongoose = require('mongoose');

const Schema =  mongoose.Schema;

const articleSchema = new Schema({
    name: { type:String, required:true },
    title: { type:String, required:true },
    subtitle: { type:String, required:true },
    author: { type:String , required:true }
});

module.exports = mongoose.model("aricle", articleSchema);