'use strict';

const mongoose = require('mongoose');

// Once started, connect to Mongo through Mongoose
mongoose.connect('mongodb://localhost:27017/authapi', {useNewUrlParser:true}, (err) =>{
    if(!err) { console.log("Mongodb connected sccessfully")}
    else{ console.log("Mongodb connetion  faild")}
})

const Glue = require('glue');
const manifest = require('./manifest')

// routes dir
const options = {
    relativeTo: __dirname
};

const startServer = async function () {
    try {
        // Start the server
        const server = await Glue.compose(manifest, options);
        await server.start();
        
        console.log(`server running at ${manifest.server.port}`);
    }
    catch (err) {
        console.error(err);
        process.exit(1);
    }
};

startServer();